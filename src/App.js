import React from 'react';
import './App.scss';

import Nav from './components/Nav/Nav';
import Recipes from "./components/Recipes/Recipes";

const App = () => (
    <div className="app" data-testid="app">
      <Nav />
      <Recipes/>
    </div>
);

export default App;
