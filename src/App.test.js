import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

const renderElement = () => (
    <App />
);

describe('App Component', () => {
    it('Renders app without crashing', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const app = getByTestId('app');
        expect(app).toBeInTheDocument();
    });
});
