import React from 'react';
import {Formik, Form, Field} from 'formik';
import validationSchema from './validationSchema';
import close from '../../images/close.svg';
import {stringToSlug} from '../../helpers';
import {createUniqueID} from '../../helpers';


const Modal = ({modalData, hideModal, updateRecipesList}) => {

    const initialValues = {
        name: modalData ? modalData.title : '',
        ingredients: modalData ? modalData.ingredients.toString() : '',
    };

    const onFormSubmit = recipe => {
        const recipeObject = {
            id: modalData && modalData.id ? modalData.id : createUniqueID(),
            slug: stringToSlug(recipe.name),
            title: recipe.name,
            ingredients: recipe.ingredients.replace(/\s*,\s*/g, ",").toLowerCase().split(',')
        };
        updateRecipesList(recipeObject, modalData.id);
    };

    return (
        <div className="modal" data-testid="modal">
            <div className="modal__content">
                <div className="modal__top">
                    <p className="modal__name">
                        <span>{modalData && modalData.type}</span> a recipe
                    </p>
                    <img
                        src={close}
                        alt="close"
                        className="modal__close"
                        onClick={() => hideModal()}
                        data-testid="close-icon"
                    />
                </div>
                <div className="modal__body">
                    <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                        onSubmit={recipe => {
                            onFormSubmit(recipe)
                        }}
                >
                        {({errors, touched}) => (
                            <Form className="modal__form">
                                <div className="form__row">
                                    <label className="form__label" htmlFor="name">Recipe</label>
                                    <Field
                                        id="name"
                                        name="name"
                                        placeholder="Recipe name"
                                    />
                                    {errors.name && touched.name &&
                                    <div className="form__errors">{errors.name}</div>}
                                </div>

                                <div className="form__row">
                                    <label className="form__label" htmlFor="ingredients">Ingredients</label>
                                    <Field
                                        id="ingredients"
                                        name="ingredients"
                                        placeholder="Enter Ingredients, Separated, By Commas"
                                        as="textarea"
                                        rows="6"
                                    />
                                    {errors.ingredients && touched.ingredients &&
                                    <div className="form__errors">{errors.ingredients}</div>}
                                </div>

                                <div className="form__buttons">
                                    <button
                                        type="submit"
                                        className="button button--primary"
                                        data-testid="submit-recipe-button"
                                    >
                                        <span>{modalData && modalData.type}</span> recipe
                                    </button>
                                    <button
                                        className="button button--secondary"
                                        onClick={() => hideModal()}
                                        data-testid="close-modal-button"
                                    >
                                        Close
                                    </button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    )
};

export default Modal;
