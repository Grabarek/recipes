import React from 'react';
import {render} from '@testing-library/react';
import Modal from './Modal';
import closeIconPath from '../../images/close.svg';

const renderElement = () => (
    <Modal/>
);

describe('Modal Component', () => {
    it('Renders modal component without crashing', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const modal = getByTestId('modal');
        expect(modal).toBeInTheDocument();
    });

    it('Renders recipe name input label', () => {
        const {getByLabelText} = render(
            renderElement()
        );

        const label = getByLabelText('Recipe');
        expect(label).toBeInTheDocument();
    });

    it('Renders recipe ingredients input label', () => {
        const {getByLabelText} = render(
            renderElement()
        );

        const label = getByLabelText('Ingredients');
        expect(label).toBeInTheDocument();
    });

    it('Renders recipe name input with default placeholder text', () => {
        const {getByPlaceholderText} = render(
            renderElement()
        );

        const input = getByPlaceholderText('Recipe name');
        expect(input).toBeInTheDocument();
    });

    it('Renders recipe ingredients input with default placeholder text', () => {
        const {getByPlaceholderText} = render(
            renderElement()
        );

        const input = getByPlaceholderText('Enter Ingredients, Separated, By Commas');
        expect(input).toBeInTheDocument();
    });

    it('Renders recipe ingredients input with default placeholder text', () => {
        const {getByPlaceholderText} = render(
            renderElement()
        );

        const input = getByPlaceholderText('Enter Ingredients, Separated, By Commas');
        expect(input).toBeInTheDocument();
    });

    it('Renders recipe ingredients input as textarea', () => {
        const {getByPlaceholderText} = render(
            renderElement()
        );

        const input = getByPlaceholderText('Enter Ingredients, Separated, By Commas');
        expect(input.tagName.toLowerCase()).toBe('textarea');
    });


    it('Renders close icon img tag', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const icon = getByTestId('close-icon');
        expect(icon.tagName.toLowerCase()).toBe('img');
    });

    it('Renders close icon img src', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const icon = getByTestId('close-icon');
        expect(icon).toHaveAttribute('src', closeIconPath);
    });

    it('Renders alt attribute on close icon', () => {
        const {getByAltText} = render(
            renderElement()
        );

        const icon = getByAltText('close');
        expect(icon).toBeInTheDocument();
    });

    it('Renders submit recipe button', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const button = getByTestId('submit-recipe-button');
        expect(button.tagName.toLowerCase()).toBe('button');
    });

    it('Renders close modal button', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const button = getByTestId('close-modal-button');
        expect(button.tagName.toLowerCase()).toBe('button');
    });
});
