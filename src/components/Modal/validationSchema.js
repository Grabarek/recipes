import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    name: Yup.string()
        .required('Recipe name is required')
        .matches(/^[a-zA-Z0-9 ]+$/, 'Recipe name can contain only letters and numbers')
        .min(3, 'Recipe name is too short')
        .max(50, 'Recipe name is too long')
    ,
    ingredients: Yup.string()
        .required('At least one ingredient is required')
        .matches(/^(\w+[, ()]+)*\w+$/, 'Seperate ingredients by using commas')
    ,
});

export default validationSchema
