import React from 'react';
import logo from '../../images/logo.svg';

const Nav = () => (
    <nav className="nav" data-testid="nav">
        <div className="container">
            <div className="nav__content">
                <img src={logo} alt="logo" className="nav__logo" data-testid="nav-logo" />
                <h1 className="nav__title" data-testid="nav-title">
                    Recipes app
                </h1>
            </div>
        </div>
    </nav>
);

export default Nav;
