import React from 'react';
import { render } from '@testing-library/react';
import Nav from './Nav';
import logoPath from '../../images/logo.svg';

const renderElement = () => (
    <Nav />
);

describe('Nav Component', () => {
    it('Renders nav without crashing', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const nav = getByTestId('nav');
        expect(nav).toBeInTheDocument();
    });

    it('Renders logo img tag', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const logo = getByTestId('nav-logo');
        expect(logo.tagName.toLowerCase()).toBe('img');
    });

    it('Renders logo src', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const logo = getByTestId('nav-logo');
        expect(logo).toHaveAttribute('src', logoPath);
    });

    it('Renders alt attribute on logo image', () => {
        const {getByAltText} = render(
            renderElement()
        );

        const logo = getByAltText('logo');
        expect(logo).toBeInTheDocument();
    });

    it('Renders h1 tag', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const title = getByTestId('nav-title');
        expect(title.tagName.toLowerCase()).toBe('h1');
    });
});
