import React from 'react';
import chevron from '../../images/chevron.svg';

const Recipe = ({id, title, ingredients, activeRecipe, toggleActiveRecipe, deleteRecipe, showModal}) => (
    <div className="recipe" data-testid="recipe">
        <div
            className={`recipe__bar ${activeRecipe === id ? 'active' : 'inactive'}`}
            onClick={() => toggleActiveRecipe(id)}
            data-testid="recipe-bar"
        >
            <h2 className="recipe__title">
                {title}
            </h2>
            <img src={chevron} alt="arrow" className="recipe__arrow"/>
        </div>
        {activeRecipe === id &&
        <div className="recipe__ingredients" data-testid="recipe-ingredients">
            <h3 className="recipe__ingredients-title">Ingredients</h3>
            <ul className="recipe__ingredients-list">
                {ingredients && ingredients.length > 0 && ingredients.map((ingredient) => (
                    <li key={ingredient}>
                        {ingredient}
                    </li>
                ))
                }
            </ul>
            <div className="recipe__ingredients-buttons">
                <button
                    className="button button--secondary"
                    onClick={() => showModal('edit', id, title, ingredients)}
                    data-testid="recipe-edit"
                >
                    Edit
                </button>
                <button
                    className="button button--warning"
                    onClick={() => deleteRecipe(id)}
                    data-testid="recipe-delete"
                >
                    Delete
                </button>
            </div>
        </div>
        }
    </div>
);

export default Recipe;
