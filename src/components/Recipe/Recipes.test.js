import React from 'react';
import {render} from '@testing-library/react';
import Recipe from './Recipe';

const renderElement = () => (
    <Recipe/>
);

describe('Recipe Component', () => {
    it('Renders recipe component without crashing', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const recipe = getByTestId('recipe');
        expect(recipe).toBeInTheDocument();
    });

    it('Renders recipe bar', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const bar = getByTestId('recipe-bar');
        expect(bar).toBeInTheDocument();
    });

    it('Renders ingredients element', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const ingredients = getByTestId('recipe-ingredients');
        expect(ingredients).toBeInTheDocument();
    });

    it('Renders recipe edit button', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const button = getByTestId('recipe-edit');
        expect(button.tagName.toLowerCase()).toBe('button');
    });

    it('Renders recipe delete button', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const button = getByTestId('recipe-delete');
        expect(button.tagName.toLowerCase()).toBe('button');
    });
});
