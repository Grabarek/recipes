import React, {useState, useEffect} from 'react';
import Recipe from "../Recipe/Recipe";
import Modal from "../Modal/Modal";
import initialRecipes from "./initialRecipes";


const Recipes = () => {
    const [recipesList, setRecipesList] = useState(initialRecipes);
    const [activeRecipe, setActiveRecipe] = useState(null);
    const [modalData, setModalData] = useState(null);

    //get recipes from localStorage on mount
    useEffect(() => {
        const recipes = window.localStorage.getItem('recipes');
        recipes && setRecipesList(JSON.parse(recipes));
    }, []);

    const toggleActiveRecipe = id => {
        activeRecipe === id ? setActiveRecipe(null) : setActiveRecipe(id)
    };

    const deleteRecipe = id => {
        const newList = recipesList.filter(recipe => recipe.id !== id);
        setRecipesList(newList);
        window.localStorage.setItem('recipes', JSON.stringify(newList));
    };

    const updateRecipesList = recipe => {
        const recipeIndex = recipesList.findIndex(item => item.id === recipe.id);
        let newArray = [];

        //update recipe if exists
        if(recipeIndex > -1) {
            newArray = [...recipesList];
            newArray[recipeIndex] = recipe;
        }
        //else add new recipe
        else {
            newArray = [
                ...recipesList,
                recipe
            ];
        }

        setRecipesList(newArray);
        window.localStorage.setItem('recipes', JSON.stringify(newArray));
        hideModal();
    };

    const showModal = (type, id, title = '', ingredients = []) => {
        setModalData({type, id, title, ingredients});
    };

    const hideModal = () => {
        setModalData(null);
    };

    return (
        <section className="recipes" data-testid="recipes">
            <div className="container">
                <div className="recipes__content">
                    {recipesList.length > 0 ? recipesList.map((recipe) => (
                            <Recipe
                                key={recipe.id}
                                id={recipe.id}
                                slug={recipe.slug}
                                title={recipe.title}
                                ingredients={recipe.ingredients}
                                activeRecipe={activeRecipe}
                                toggleActiveRecipe={() => toggleActiveRecipe(recipe.id)}
                                deleteRecipe={(id) => deleteRecipe(id)}
                                showModal={(type, id, title, ingredients) => showModal(type, id, title, ingredients)}
                            />
                        ))
                        :
                        <p className="recipes__empty">
                            Add your first recipe..
                        </p>
                    }
                </div>
                <button
                    className="recipes__add button button--primary"
                    onClick={() => showModal('add')}
                >
                    Add recipe
                </button>
            </div>

            {modalData && (
                <Modal
                    modalData={modalData}
                    hideModal={() => hideModal()}
                    updateRecipesList={(recipe) => updateRecipesList(recipe)
                    }
                />
            )}
        </section>
    )
};

export default Recipes;
