import React from 'react';
import { render } from '@testing-library/react';
import Recipes from './Recipes';

const renderElement = () => (
    <Recipes />
);

describe('Recipes Component', () => {
    it('Renders recipes component without crashing', () => {
        const {getByTestId} = render(
            renderElement()
        );

        const recipes = getByTestId('recipes');
        expect(recipes).toBeInTheDocument();
    });

    it('Renders three mocked up recipe tabs', () => {
        const {getAllByTestId} = render(
            renderElement()
        );

        const recipe = getAllByTestId('recipe');
        expect(recipe.length).toBe(3);
    });

    it('Renders add recipe button', () => {
        const {getByText} = render(
            renderElement()
        );

        const button = getByText('Add recipe');
        expect(button).toBeInTheDocument();
    });

});
