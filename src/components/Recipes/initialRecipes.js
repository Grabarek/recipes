const initialRecipes = [
    {
        id: 119405077139945448995,
        slug: 'cinnamon-roll-pancakes',
        title: 'Cinnamon Roll Pancakes',
        ingredients: ['milk', 'flour', 'salt', '(optional) sugar', 'baking soda']
    },
    {
        id: 151345077131945448995,
        slug: 'lasagna',
        title: 'Lasagna',
        ingredients: ['sausage', 'beef', 'onion', 'tomato']
    },
    {
        id: 129145077131945448995,
        slug: 'cinnamon-roll-poke-cake',
        title: 'Cinnamon Roll Poke Cake',
        ingredients: ['cooking spray', 'melted butter', '(optional) brown sugar', 'cinnamon']
    }
];

export default initialRecipes;
